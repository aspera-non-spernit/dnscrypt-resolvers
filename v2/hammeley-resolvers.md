# public-resolvers

This list of public DNS resolvers support the
DNSCrypt protocols.

The servers and this list is
maintained by Daniel Hammeley <daniel @ hammeley [.] info>

To use this list, add the following to the `[sources]` section of your
`dnscrypt-proxy.toml` configuration file:

    [sources.'hammeley-resolvers']
    urls = ['https://gitlab.com/aspera-non-spernit/dnscrypt-resolvers/raw/master/v2/hammeley-resolvers.md']
    minisign_key = 'RWQZ8yq2PM6DFXHNysoL5Br1ByKkbiUELLhqEIZ3nijGtjpn42np5LUT'
    cache_file = '/var/cache/dnscrypt-proxy/hammeley-resolvers.md'
    prefix = 'hammeley-' 

--

## hammeley.info
DNSCrypt protocol. Non-Logging, Non-Filtering, DNSSEC support. 

Running on a Cloud Server located in North-West France. Uses a European Upstream Server.

Maintained by https://hammeley.info

sdns://AQcAAAAAAAAAETUxLjE3OC42My4yNDY6NDQzIHFJNLmDIJOrOfx10mQnyCchGHa6OwktdMUOjt3Uz19nHTIuZG5zY3J5cHQtY2VydC5oYW1tZWxleS5pbmZv
