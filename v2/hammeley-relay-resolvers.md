# relay-resolvers

This list of public DNS resolvers support the
DNSCrypt protocols.

The servers and this list is
maintained by Daniel Hammeley <daniel @ hammeley [.] info>

To use this list, add the following to the `[sources]` section of your
`dnscrypt-proxy.toml` configuration file:

  ## Anonymized Relay Resolvers maintained by hammeley.info

  [sources.'hammeley-relay-resolvers']
  urls = ['https://gitlab.com/aspera-non-spernit/dnscrypt-resolvers/raw/master/v2/hammeley-relay-resolvers.md']
  minisign_key = 'RWQZ8yq2PM6DFXHNysoL5Br1ByKkbiUELLhqEIZ3nijGtjpn42np5LUT'
  cache_file = '/var/cache/dnscrypt-proxy/hammeley-relay-resolvers.md'
  prefix = 'hammeley-relay' 

--

## Anonymized DNS Relay
DNSCrypt protocol. Non-Logging, Non-Filtering, DNSSEC support. 

Running on a Cloud Server located in North-West France. 

Maintained by hammeley.info

sdns://gQswLjAuMC4wOjQ0Mw
